#include <random>

#ifndef UTILITIES_HPP
#define UTILITIES_HPP

class Utilities
{
public:
    static size_t RandomizeUInt( const size_t& a, const size_t& b );
    static double RandomizeReal( const double& a, const double& b );

    static void UpdateMouseState();
    static int32_t GetMouseX();
    static int32_t GetMouseY();

private:
    static std::default_random_engine e;
    static int32_t mouseX, mouseY;
};
#endif
