#include <memory>
#include <thread>

#ifndef LIFE_HPP
#define LIFE_HPP

class Grid;

class Life
{
public:
    Life( std::shared_ptr< Grid > );
    ~Life();

    void Update();

private:
    std::shared_ptr< Grid > m_gridPtr;
    class CellThread;
};

#endif
