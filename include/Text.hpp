#include <string>
#include <vector>
#include <SDL2/SDL_ttf.h>

#ifndef TEXT_HPP
#define TEXT_HPP

struct SDL_Rect;
struct SDL_Texture;
struct SDL_Renderer;

class Text
{
public:
    Text( SDL_Renderer*, const std::string&, const size_t& );
    ~Text();

    bool WriteText(
        const std::string&,
        const int32_t&, const int32_t&,
        const SDL_Color& = { 255, 255, 255, 255 } );
    void Draw();

private:
    struct FontTexture;
    std::vector< FontTexture > m_textures;

    std::string m_fontName;
    std::string m_text;

    uint16_t m_fontSize;
    int32_t m_fontX, m_fontY;

    TTF_Font* m_font;
    SDL_Renderer* m_renderer;
};

#endif
