#include <vector>
#include <utility>
#include <cstdint>
#include <memory>

#ifndef GRID_HPP
#define GRID_HPP

struct SDL_Renderer;

enum class Cell { Dead, Alive };

class Grid
{
public:
    Grid( SDL_Renderer*,
        const int32_t&, const int32_t&, const int32_t&,
        const int32_t& = 0, const int32_t& = 0 );
    ~Grid();

    std::vector< Cell >& GetGrid();
    void SetGrid( const std::vector< Cell >& );
    int32_t GetWidth() const { return m_width; }
    int32_t GetHeight() const { return m_height; }
    void Draw( const bool& );
    void Update();
    void PlaceCell();
    void FillRandom();
    void Clear();
    
private:
    struct Point
    {
        int32_t x, y;
    };
    std::vector< std::pair< Point, Point > > m_gridLines;
    std::vector< Cell > m_gridContent;
    SDL_Renderer* m_renderer;

    int32_t m_width, m_height;
    int32_t m_tileSize;
    int32_t m_pixelWidth, m_pixelHeight;

    int32_t m_originX, m_originY;
};

#endif
