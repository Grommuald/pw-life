#include <functional>
#include <chrono>

#ifndef TIMER_HPP
#define TIMER_HPP

class Timer {
public:
	Timer();

	void ExecuteAfterTime( const uint64_t&, std::function<void()> );
	void Reset();

	bool HasBeenSet() const { return m_hasBeenSet; }
	bool HasBeenStopped() const { return m_stopTimer; }

private:
	int64_t m_timeToExecute;

	std::chrono::system_clock::time_point m_currentTime;
	std::chrono::system_clock::time_point m_elapsedTime;
	bool m_hasBeenSet;
	bool m_stopTimer;
};

#endif
