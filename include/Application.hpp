#ifndef LIFE_APP_HPP
#define LIFE_APP_HPP

#include <memory>
#include <string>

struct SDL_Renderer;
struct SDL_Window;
struct SDL_Surface;
struct SDL_Texture;
class Text;
class Timer;
class Grid;
class Life;
class Button;

class Application
{
public:
	Application();
	~Application();

	void Run();

private:
	static const size_t m_tileSize { 16 };
	static const size_t m_gridWidth { 36 };
	static const size_t m_gridHeight { 36 };

	static const size_t m_screenWidth { 576 };
	static const size_t m_screenHeight { 648 };

    static const int32_t m_gridOffsetX { 0 };
    static const int32_t m_gridOffsetY { 72 };

    static const uint64_t m_updateRefreshRateInMillis { 500 };

	static constexpr const char* m_windowCaption { "Life" };

	SDL_Renderer* m_renderer;
	SDL_Window* m_window;

    std::shared_ptr< Grid > m_grid;
    std::unique_ptr< Life > m_life;
    std::unique_ptr< Timer > m_refreshTimer;
    std::unique_ptr< Button > m_runButton, m_randomizeButton, m_clearButton;
    std::unique_ptr< Text > m_statusLabelText, m_statusValueText;
    
    bool m_isProcessingLife { false };
    bool m_mouseButtonPressed { false };

	void Draw();
	void Update();
    void DrawGrid();
};
#endif
