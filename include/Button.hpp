#include <functional>
#include <memory>
#include <string>
#include <SDL2/SDL.h>

#ifndef BUTTON_HPP
#define BUTTON_HPP

class Text;

enum class ButtonState { None, Pressed };

class Button
{
public:
    Button(
        SDL_Renderer* renderer,
        const int32_t&, const int32_t&,
        const int32_t&, const int32_t&,
        const std::string&,
        const SDL_Color&,
        std::function< void() > );

    void Update();
    void SetState( const ButtonState& );
    void Draw();
    void SetActive( const bool& );
    void SetText( const std::string&, const SDL_Color& = { 255, 255, 255, 255 } );

private:
    int32_t m_posX, m_posY;
    int32_t m_width, m_height;

    std::unique_ptr< Text > m_caption;
    std::function< void() > m_action;

    bool m_isActive { true };

    SDL_Renderer* m_renderer;
    SDL_Color m_color, m_borderColor;

    ButtonState m_previousState { ButtonState::None };
    ButtonState m_currentState { ButtonState::None };
};

#endif
