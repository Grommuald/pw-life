OUT = bin/life
CC = g++
IFLAGS = -Iinclude
CFLAGS = -g -O0 -Wall -std=c++14
LDFLAGS = -lGL -lSDL2 -lSDL2_gfx -lSDL2_ttf -lpthread
SOURCES = src/*

OBJECTS = $(SOURCES:.c=.o)

all:
	mkdir bin
	$(CC) $(IFLAGS) $(CFLAGS) -o $(OUT) $(SOURCES) $(LDFLAGS)
clean:
	rm -rf bin
