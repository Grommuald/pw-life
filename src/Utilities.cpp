#include "Utilities.hpp"
#include <SDL2/SDL.h>
#include <random>

std::default_random_engine Utilities::e( std::random_device{}() );
int32_t Utilities::mouseX;
int32_t Utilities::mouseY;

size_t Utilities::RandomizeUInt( const size_t& a, const size_t& b )
{
    std::uniform_int_distribution< size_t > u( a, b );
    return u( e );
}

double Utilities::RandomizeReal( const double& a, const double& b )
{
    std::uniform_real_distribution< double > u( a, b );
    return u( e );
}

void Utilities::UpdateMouseState()
{
    SDL_GetMouseState( &mouseX, &mouseY );
}

int32_t Utilities::GetMouseX()
{
    return mouseX;
}

int32_t Utilities::GetMouseY()
{
    return mouseY;
}
