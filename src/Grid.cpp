#include "Grid.hpp"
#include "Utilities.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <iostream>

Grid::Grid(
    SDL_Renderer* renderer,
    const int32_t& width, const int32_t& height,
    const int32_t& tileSize,
    const int32_t& originX, const int32_t& originY )
    :
    m_gridContent( width * height ),
    m_renderer( renderer ),
    m_width( width ),
    m_height( height ),
    m_tileSize( tileSize ),
    m_pixelWidth( width * tileSize ),
    m_pixelHeight( height * tileSize ),
    m_originX( originX ),
    m_originY( originY )
{
    m_gridLines.push_back(
        std::make_pair(
            Point { m_originX, m_originY },
            Point { m_originX, m_originY + m_pixelHeight }
        )
    );
    m_gridLines.push_back(
        std::make_pair(
            Point { m_originX, m_originY },
            Point { m_originX + m_pixelWidth, m_originY }
        )
    );
    m_gridLines.push_back(
        std::make_pair(
            Point { m_originX + m_pixelWidth, m_originY },
            Point { m_originX + m_pixelWidth, m_originY + m_pixelHeight }
        )
    );
    m_gridLines.push_back(
        std::make_pair(
            Point { m_originX, m_originY  + m_pixelHeight },
            Point { m_originX + m_pixelWidth, m_originY + m_pixelHeight }
        )
    );

    for ( int32_t i = 1; i < m_height; ++i )
        m_gridLines.push_back(
            std::make_pair(
                Point { m_originX, m_originY + i * m_tileSize },
                Point { m_originX + m_pixelWidth, m_originY + i * m_tileSize }
            )
        );

    for ( int32_t i = 1; i < m_width; ++i )
        m_gridLines.push_back(
            std::make_pair(
                Point { m_originX + i * m_tileSize, m_originY },
                Point { m_originX + i * m_tileSize, m_originY + m_pixelHeight }
            )
        );

    for ( auto& i : m_gridContent )
        i = Cell::Dead;
}

Grid::~Grid()
{
    std::cout << "Grid.cpp: ~Grid: begin\n";
    std::cout << "Grid.cpp: ~Grid: end\n";
}

std::vector< Cell >& Grid::GetGrid()
{
    return m_gridContent;
}

void Grid::SetGrid( const std::vector< Cell >& grid )
{
    m_gridContent = grid;
}

void Grid::Draw( const bool& isProccessing )
{
    for ( const auto& i : m_gridLines )
        lineRGBA( m_renderer,
            i.first.x, i.first.y,
            i.second.x, i.second.y,
            255, 0, 0, 100
        );

    if ( !isProccessing
        && Utilities::GetMouseX() >= m_originX
        && Utilities::GetMouseY() >= m_originY
        && Utilities::GetMouseX() < m_pixelWidth + m_originX
        && Utilities::GetMouseY() < m_pixelHeight + m_originY )
    {
        int32_t mouseOffsetX = m_originX % m_tileSize;
        int32_t mouseOffsetY = m_originY % m_tileSize;

        std::pair< Point, Point > cellRect
            = std::make_pair(
                Point {
                    ( ( Utilities::GetMouseX() - mouseOffsetX ) / m_tileSize ) * m_tileSize + 1 + mouseOffsetX,
                    ( ( Utilities::GetMouseY() - mouseOffsetY ) / m_tileSize ) * m_tileSize + 1 + mouseOffsetY
                },
                Point {
                    ( ( Utilities::GetMouseX() - mouseOffsetX ) / m_tileSize ) * m_tileSize + m_tileSize - 1 + mouseOffsetX,
                    ( ( Utilities::GetMouseY() - mouseOffsetY ) / m_tileSize ) * m_tileSize + m_tileSize - 1 + mouseOffsetY }
            );
        boxRGBA( m_renderer,
                cellRect.first.x, cellRect.first.y,
                cellRect.second.x, cellRect.second.y,
                255, 135, 35, 255 );
    }

    for ( int32_t i = 0; i < ( int32_t ) m_gridContent.size(); ++i )
        if ( m_gridContent[i] == Cell::Alive )
        {
            int32_t tileX = ( i % m_width ) * m_tileSize + m_originX;
            int32_t tileY = ( i / m_width ) * m_tileSize + m_originY;

            boxRGBA( m_renderer,
                tileX + 1, tileY + 1,
                tileX + m_tileSize - 1, tileY + m_tileSize - 1,
                103, 126, 208, 255 );
        }
}

void Grid::Update()
{

}

void Grid::PlaceCell()
{
    if ( Utilities::GetMouseX() >= m_originX
        && Utilities::GetMouseY() >= m_originY
        && Utilities::GetMouseX() < m_pixelWidth + m_originX
        && Utilities::GetMouseY() < m_pixelHeight + m_originY )
    {
        int32_t row = ( Utilities::GetMouseY() - m_originY ) / m_tileSize;
        int32_t col = ( Utilities::GetMouseX() - m_originX ) / m_tileSize;

        m_gridContent[ col + row * m_width ]
            = m_gridContent[ col + row * m_width ] == Cell::Dead ? Cell::Alive : Cell::Dead;
    }
}

void Grid::FillRandom()
{
    for ( auto& i : m_gridContent )
        i = Utilities::RandomizeUInt( 0, 1 ) ? Cell::Alive : Cell::Dead;
}

void Grid::Clear()
{
    for ( auto& i : m_gridContent )
        i = Cell::Dead;
}
