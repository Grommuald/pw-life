#include "Life.hpp"
#include "Grid.hpp"
#include <iostream>
#include <cassert>

class Life::CellThread
{
public:
    CellThread(
        const int32_t&, const int32_t&,
        const int32_t&, const int32_t&,
        const std::vector< Cell >&,
        std::vector< Cell >& );

    void Run();
    std::thread& GetThread() { return m_thread; }

private:
    std::thread m_thread;
    int32_t m_row, m_col;
    int32_t m_gridWidth, m_gridHeight;
    const std::vector< Cell >& m_previousGridState;
    std::vector< Cell >& m_currentGridState;
};

Life::CellThread::CellThread(
    const int32_t& row,
    const int32_t& column,
    const int32_t& gridWidth,
    const int32_t& gridHeight,
    const std::vector< Cell >& previousGridState,
    std::vector< Cell >& currentGridState )
    :
    m_row( row ),
    m_col( column ),
    m_gridWidth( gridWidth ),
    m_gridHeight( gridHeight ),
    m_previousGridState( previousGridState ),
    m_currentGridState( currentGridState )
{

}

void Life::CellThread::Run()
{
    m_thread = std::thread(
    [ this ]
    {
        size_t numberOfNeighbours = 0;

        if ( m_row - 1 >= 0 && m_previousGridState[m_col + ( m_row-1 )*m_gridWidth] == Cell::Alive )
            ++numberOfNeighbours;

        if ( m_col - 1 >= 0 && m_previousGridState[m_col - 1 + m_row*m_gridWidth] == Cell::Alive )
             ++numberOfNeighbours;

        if ( m_col + 1 < m_gridWidth && m_previousGridState[m_col + 1 + m_row*m_gridWidth] == Cell::Alive )
             ++numberOfNeighbours;

        if ( m_row + 1 < m_gridHeight && m_previousGridState[m_col + ( m_row+1 )*m_gridWidth] == Cell::Alive )
             ++numberOfNeighbours;

        if ( m_row - 1 >= 0 && m_col - 1 >= 0
            && m_previousGridState[m_col-1 + ( m_row-1 )*m_gridWidth] == Cell::Alive )
             ++numberOfNeighbours;

        if ( m_row - 1 >= 0 && m_col + 1 < m_gridWidth
            && m_previousGridState[m_col+1 + ( m_row-1 )*m_gridWidth] == Cell::Alive )
             ++numberOfNeighbours;

        if ( m_row + 1 < m_gridHeight && m_col - 1 >= 0
            && m_previousGridState[m_col-1 + ( m_row+1 )*m_gridWidth] == Cell::Alive )
             ++numberOfNeighbours;

        if ( m_row + 1 < m_gridHeight && m_col + 1 < m_gridWidth
            && m_previousGridState[m_col+1 + ( m_row+1 )*m_gridWidth] == Cell::Alive )
             ++numberOfNeighbours;

        if ( m_previousGridState[m_col + m_row*m_gridWidth] == Cell::Alive )
        {
            if ( numberOfNeighbours < 2 || numberOfNeighbours > 3 )
                m_currentGridState[m_col + m_row*m_gridWidth] = Cell::Dead;
            else
                m_currentGridState[m_col + m_row*m_gridWidth] = Cell::Alive;
        }
        else if ( m_previousGridState[m_col + m_row*m_gridWidth] == Cell::Dead )
        {
            if ( numberOfNeighbours == 3 )
                m_currentGridState[m_col + m_row*m_gridWidth] = Cell::Alive;
        }
    } );
}

Life::Life( std::shared_ptr< Grid > grid )
    :
    m_gridPtr( grid )
{

}

Life::~Life()
{
    std::cout << "Life.cpp: ~Life: begin\n";
    std::cout << "Life.cpp: ~Life: end\n";
}

void Life::Update()
{
    std::vector< Cell > result( m_gridPtr->GetWidth() * m_gridPtr->GetHeight() );
    std::vector< CellThread > cellThreads;

    for ( int32_t i = 0; i < m_gridPtr->GetHeight(); ++i )
    {
        for ( int32_t j = 0; j < m_gridPtr->GetWidth(); ++j )
        {
            cellThreads.emplace_back(
                i, j,
                m_gridPtr->GetWidth(), m_gridPtr->GetHeight(),
                m_gridPtr->GetGrid(), result );
        }
    }

    for ( auto& i : cellThreads )
        i.Run();

    for ( auto& i : cellThreads )
        i.GetThread().join();

    m_gridPtr->SetGrid( result );
}
