#include "Text.hpp"
#include <SDL2/SDL.h>
#include <iostream>

struct Text::FontTexture
{
    SDL_Texture* texture;
    SDL_Rect rect;
};

Text::Text( SDL_Renderer* renderer,
    const std::string& fontName, const size_t& fontSize )
    :
    m_fontName( fontName ),
    m_fontSize( fontSize ),
    m_renderer( renderer )
{

}

Text::~Text()
{
    std::cout << "Text.cpp: ~Text: begin\n";
    for ( auto& i : m_textures )
        SDL_DestroyTexture( i.texture );
    std::cout << "Text.cpp: ~Text: end\n";
}

bool Text::WriteText(
    const std::string& text,
    const int32_t& x, const int32_t& y,
    const SDL_Color& color )
{
    uint32_t lastOccur = 0, nextRowY = y;
    std::vector< std::string > pieces;

    if ( !m_textures.empty() )
        m_textures.clear();

    for ( uint32_t i = 0; i < text.length(); ++i )
    {
        if ( text[i] == '\n')
        {
            pieces.push_back( text.substr( lastOccur, i ) );
            lastOccur = i+1;
        }
    }
    if ( lastOccur < text.length() )
        pieces.push_back( text.substr( lastOccur, text.length() - lastOccur ) );

    if ( ! ( m_font = TTF_OpenFont( m_fontName.c_str(), m_fontSize ) ) )
    {
        std::cout << "Unable to load font: " << TTF_GetError() << "\n";
        return true;
    }

    for ( const auto& i : pieces )
    {
        SDL_Surface* fontSurface =
            TTF_RenderText_Solid( m_font, i.c_str(), color );

        m_textures.push_back(
            { SDL_CreateTextureFromSurface( m_renderer, fontSurface ) }
        );
        SDL_FreeSurface( fontSurface );

        FontTexture& last = m_textures.back();
        SDL_QueryTexture(
            last.texture,
            nullptr, nullptr,
            &(last.rect.w), &(last.rect.h) );

        last.rect.x = x;
        last.rect.y = nextRowY;
        nextRowY += m_fontSize;

    }
    TTF_CloseFont( m_font );

    m_text = text;
    m_fontX = x;
    m_fontY = y;

    return false;
}

void Text::Draw()
{
    for ( auto& i : m_textures )
        SDL_RenderCopy( m_renderer, i.texture, nullptr, &i.rect );
}
