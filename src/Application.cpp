#include "Application.hpp"
#include "Grid.hpp"
#include "Text.hpp"
#include "Timer.hpp"
#include "Life.hpp"
#include "Utilities.hpp"
#include "Button.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <iostream>

const size_t Application::m_tileSize;
const size_t Application::m_gridWidth;
const size_t Application::m_gridHeight;

const size_t Application::m_screenWidth;
const size_t Application::m_screenHeight;

const int32_t Application::m_gridOffsetX;
const int32_t Application::m_gridOffsetY;

const uint64_t Application::m_updateRefreshRateInMillis;

constexpr const char* Application::m_windowCaption;

Application::Application()
{
	std::cout << "Application.cpp: Application: start\n";
	if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
		exit( 1 );

    auto SDLQuitAndExit = []
    {
        SDL_Quit();
        exit( 1 );
    };

	if ( !( m_window = SDL_CreateWindow(
		Application::m_windowCaption,
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		Application::m_screenWidth,
		Application::m_screenHeight,
		0
	) ) )
    {
        SDLQuitAndExit();
    }

	if ( !( m_renderer = SDL_CreateRenderer( m_window, -1, 0 ) ) )
		SDLQuitAndExit();

    if ( TTF_Init() == -1 )
    {
        std::cout << "TTF_Error: " << TTF_GetError() << "\n";
        SDLQuitAndExit();
    }
    m_grid = std::make_shared< Grid >(
        m_renderer, m_gridWidth, m_gridHeight, m_tileSize, m_gridOffsetX, m_gridOffsetY );
    m_refreshTimer = std::make_unique< Timer >();
    m_life = std::make_unique< Life >( m_grid );

    m_statusLabelText = std::make_unique< Text >( m_renderer, "res/freeroad.ttf", 16 );
    m_statusValueText = std::make_unique< Text >( m_renderer, "res/freeroad.ttf", 16 );

    m_statusLabelText->WriteText( "Simulation status: ", 400, 10 );
    m_statusValueText->WriteText( "Stopped", 400, 30, { 255, 0, 0, 255 } );

    m_runButton = std::make_unique< Button >(
        m_renderer, 10, 10, 100, 40, "Run", SDL_Color { 0, 255, 0, 255 },
        [ this ]
        {
            m_isProcessingLife = !m_isProcessingLife;
            if ( m_isProcessingLife )
            {
                m_clearButton->SetActive( false );
                m_randomizeButton->SetActive( false );
                m_runButton->SetText( "Stop", { 255, 0, 0, 255 } );
                m_statusValueText->WriteText( "Running", 400, 30, { 0, 255, 0, 255 } );
            }
            else
            {
                m_clearButton->SetActive( true );
                m_randomizeButton->SetActive( true );
                m_runButton->SetText( "Run", { 0, 255, 0, 255 } );
                m_statusValueText->WriteText( "Stopped", 400, 30, { 255, 0, 0, 255 } );
            }
        } );

    m_randomizeButton = std::make_unique< Button >(
        m_renderer, 120, 10, 100, 40, "Rand", SDL_Color { 255, 255, 255, 255 },
        [ this ]
        {
            m_grid->FillRandom();
        } );
    m_clearButton = std::make_unique< Button >(
        m_renderer, 230, 10, 100, 40, "Clear", SDL_Color { 255, 255, 255, 255 },
        [ this ]
        {
            m_grid->Clear();
        } );

	std::cout << "Application.cpp: Application: end\n";
}

Application::~Application()
{
    std::cout << "Application.cpp: ~Application: begin\n";
	if ( m_renderer )
		SDL_DestroyRenderer( m_renderer );
	if ( m_window )
		SDL_DestroyWindow( m_window );

    TTF_Quit();
	SDL_Quit();
    std::cout << "Application.cpp: ~Application: end\n";
}

void Application::Run()
{
	Update();
}

void Application::Draw()
{
	SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, SDL_ALPHA_OPAQUE );
	SDL_RenderClear( m_renderer );
	SDL_SetRenderDrawColor( m_renderer, 255, 255, 255, SDL_ALPHA_OPAQUE );

    m_grid->Draw( m_isProcessingLife );

    m_runButton->Draw();
    m_randomizeButton->Draw();
    m_clearButton->Draw();
    m_statusLabelText->Draw();
    m_statusValueText->Draw();

    SDL_RenderPresent( m_renderer );
}

void Application::Update()
{
	std::cout << "Application.cpp: Application::update: start\n";
	bool done = false;

	while ( !done )
	{
        Utilities::UpdateMouseState();

		SDL_Event event;
		while ( SDL_PollEvent( &event ) )
		{
            switch ( event.type )
            {
                case SDL_QUIT:
                    done = true;
                    break;

                case SDL_KEYDOWN:
                {
                    switch ( event.key.keysym.sym )
                    {
                        case SDLK_SPACE:
                            m_isProcessingLife = !m_isProcessingLife;
                        break;

                        case SDLK_r:
                            if ( !m_isProcessingLife )
                                m_grid->FillRandom();
                        break;
                    }
                }
                break;

                case SDL_MOUSEBUTTONDOWN:
                    m_grid->PlaceCell();
                    m_runButton->SetState( ButtonState::Pressed );
                    m_randomizeButton->SetState( ButtonState::Pressed );
                    m_clearButton->SetState( ButtonState::Pressed );
                break;
            }
			if ( event.type == SDL_QUIT )
				done = true;
		}

        m_runButton->Update();
        m_randomizeButton->Update();
        m_clearButton->Update();

        if ( m_isProcessingLife )
        {
            m_refreshTimer->ExecuteAfterTime( m_updateRefreshRateInMillis,
            [this]
            {
                m_life->Update();
            } );
        }
		Draw();
	}
}
