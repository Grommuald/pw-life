#include "Button.hpp"
#include "Text.hpp"
#include "Utilities.hpp"

#include <SDL2/SDL2_gfxPrimitives.h>
#include <iostream>

Button::Button(
    SDL_Renderer* renderer,
    const int32_t& posX, const int32_t& posY,
    const int32_t& width, const int32_t& height,
    const std::string& caption,
    const SDL_Color& textColor,
    std::function< void() > onTrigger )
    :
    m_posX( posX ),
    m_posY( posY ),
    m_width( width ),
    m_height( height ),
    m_action( onTrigger ),
    m_renderer( renderer ),
    m_color { 160, 160, 160, 255 },
    m_borderColor { 100, 100, 100, 255 }
{
    m_caption = std::make_unique< Text >( m_renderer, "res/freeroad.ttf", 16 );
    m_caption->WriteText( caption, m_posX + m_width/3, m_posY + m_height/3, textColor );
}

void Button::SetText( const std::string& caption, const SDL_Color& textColor )
{
    m_caption->WriteText( caption, m_posX + m_width/3, m_posY + m_height/3, textColor );
}

void Button::Update()
{
    if ( m_isActive )
    {
        if ( Utilities::GetMouseX() >= m_posX
            && Utilities::GetMouseX() <= m_posX + m_width
            && Utilities::GetMouseY() >= m_posY
            && Utilities::GetMouseY() <= m_posY + m_height )
        {
            m_color = { 130, 130, 130, 255 };
        }
        else
            m_color = { 160, 160, 160, 255 };

        if ( m_previousState != m_currentState )
        {
            m_action();
            m_currentState = m_previousState = ButtonState::None;
        }
        else
            m_previousState = m_currentState;
    }
}

void Button::SetState( const ButtonState& state )
{
    if ( Utilities::GetMouseX() >= m_posX
        && Utilities::GetMouseX() <= m_posX + m_width
        && Utilities::GetMouseY() >= m_posY
        && Utilities::GetMouseY() <= m_posY + m_height )
    {
        if ( m_isActive )
            m_currentState = state;
    }
}

void Button::SetActive( const bool& value )
{
    m_isActive = value;
}

void Button::Draw()
{
    boxRGBA( m_renderer,
            m_posX, m_posY,
            m_posX + m_width, m_posY + m_height,
            m_color.r, m_color.g, m_color.b, m_isActive ? m_color.a : 100 );
    thickLineRGBA (m_renderer,
        m_posX, m_posY,
        m_posX + m_width, m_posY,
        2, m_borderColor.r, m_borderColor.g, m_borderColor.b, m_isActive ? m_borderColor.a : 100 );
    thickLineRGBA (m_renderer,
        m_posX, m_posY,
        m_posX, m_posY + m_height,
        2, m_borderColor.r, m_borderColor.g, m_borderColor.b, m_isActive ? m_borderColor.a : 100 );
    thickLineRGBA (m_renderer,
        m_posX, m_posY + m_height,
        m_posX + m_width, m_posY + m_height,
        2, m_borderColor.r, m_borderColor.g, m_borderColor.b, m_isActive ? m_borderColor.a : 100 );
    thickLineRGBA (m_renderer,
        m_posX + m_width, m_posY,
        m_posX + m_width, m_posY + m_height,
        2, m_borderColor.r, m_borderColor.g, m_borderColor.b, m_isActive ? m_borderColor.a : 100 );
    m_caption->Draw();
}
